/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts,jsx}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './node_modules/tw-elements/dist/js/**/*.js',
    // "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      fontFamily: {
        roboto: ['Roboto', 'sans-serif']
      },
      colors: {
        primary: '#ee4d2d',
        secondary: '#000000',
      },
    },
  },
  plugins: [
    require('tw-elements/dist/plugin')
  ],
}
