import { reactive, toRefs, defineComponent, onMounted, computed } from "vue";
import AuthServices from "@/firebase/services/auth";
export default defineComponent({
  setup() {
    const { $auth } = useNuxtApp()
    const router = useRouter()
    const authServices = new AuthServices($auth)
    const state = reactive({
      showNavbar: false,
      menus: [
        {
          name: 'Home',
          href: '#'
        },
        {
          name: 'User',
          href: '/user'
        },
        {
          name: 'Capabilities',
          href: '#'
        },
        {
          name: 'Projects',
          href: '#'
        },
      ],
      user: null
    })

    const toggleNavbar = () => {
      state.showNavbar = !state.showNavbar
    }

    const handleLogout = async () => {
      try {
        await authServices.logout()
        router.push('/')
      } catch (error) {
        console.log('handleLogout failed ==> ', error)
      }
    }

    onMounted(() => {
      authServices.onAuthStateChanged(user => {
        state.user = user
      })
    })

    return {
      ...toRefs(state),
      toggleNavbar,
      handleLogout
    }
  }
})