import {
  RecaptchaVerifier,
  signInWithPhoneNumber,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
  signInAnonymously,
  onAuthStateChanged,
  signOut,
  sendEmailVerification,
} from 'firebase/auth'
import UserServices from '@/firebase/services/user'
import {
  Timestamp
} from 'firebase/firestore'
class AuthServices {
  constructor(_auth) {
    this.auth = _auth
  }
  onAuthStateChanged(cb) {
    return this.auth ? onAuthStateChanged(this.auth, cb) : cb
  }

  getCurrentUser() {
    const currentUser = this.auth.currentUser
    return currentUser
  }

  async register(email, password) {
    try {
      const res = await createUserWithEmailAndPassword(this.auth, email, password)
      await sendEmailVerification(res.user).then(res => console.log('test')).catch('send verify')
      return res
    } catch (error) {
      console.error('register failed ==> ', error)
    }

  }

  loginWithEmail(email, password) {
    return signInWithEmailAndPassword(this.auth, email, password)
  }

  resetPassword(email) {
    return sendPasswordResetEmail(auth, email)
  }

  logout() {
    return signOut(this.auth)
  }
}

export default AuthServices
