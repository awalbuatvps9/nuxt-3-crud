import { defineNuxtPlugin } from '#app'
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getAuth, } from 'firebase/auth'

export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig();
  const firebaseConfig = {
    apiKey: config.NUXT_FIREBASE_API_KEY,
    authDomain: config.NUXT_FIREBASE_AUTH_DOMAIN,
    projectId: config.NUXT_FIREBASE_PROJECT_ID,
    storageBucket: config.NUXT_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: config.NUXT_FIREBASE_MESSAGING_SENDER_ID,
    appId: config.NUXT_FIREBASE_APP_ID,
    measurementId: config.NUXT_FIREBASE_MEASUREMENT_ID,
  };
  const firebaseDev = initializeApp(firebaseConfig)
  const db = getFirestore(firebaseDev)
  const auth = getAuth(firebaseDev)
  auth.languageCode = 'vi'
  return {
    provide: {
      auth,
      db
    }
  }
})